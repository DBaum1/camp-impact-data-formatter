package application;

import javafx.scene.control.Alert;
import model.Camper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.Locale;
import java.util.TreeSet;

/**
 * Writes Location,	First Name, Last Name, Medical condition, Medication
 * columns and data (hardcoded)
 */
public class AllergyDataFormatter implements ExcelFileFormatter {
    private final TreeSet<Camper> campers;
    private final XSSFWorkbook workbook;
    private final XSSFCellStyle defaultCellStyle;
    private final XSSFCellStyle titleCellStyle;

    private static final int NUM_COLUMNS = 5;
    private static final int LOCATION_COLUMN = 0;
    private static final int FIRST_NAME_COLUMN = 1;
    private static final int LAST_NAME_COLUMN = 2;
    private static final int MEDICAL_CONDITION_COLUMN = 3;
    private static final int MEDICATION_COLUMN = 4;
    private final String LOG_TAG = this.getClass().getName();

    public AllergyDataFormatter(XSSFWorkbook workbook,
                                XSSFCellStyle defaultCellStyle,
                                XSSFCellStyle titleCellStyle,
                                TreeSet<Camper> campers) {
        this.workbook = workbook;
        this.defaultCellStyle = defaultCellStyle;
        this.titleCellStyle = titleCellStyle;
        this.campers = campers;
    }

    @Override
    public void writeToFile() {
        XSSFSheet allergiesSheet = Utils.createNewSheet
                (workbook, "Allergies");
        writeHeaders(allergiesSheet);
        writeCamperAllergies(allergiesSheet);
        Utils.autoSizeColumns(allergiesSheet, NUM_COLUMNS);
    }

    private void writeHeaders(XSSFSheet allergiesSheet) {
        Row titleRow = allergiesSheet.createRow(0);
        Utils.writeStringCell(titleCellStyle, titleRow,
                0, "Medical Information");
        allergiesSheet.addMergedRegion(
                new CellRangeAddress(0, 0, 0, NUM_COLUMNS - 1));

        Row dataHeaderRow = allergiesSheet.createRow(1);
        Utils.writeStringCell(titleCellStyle, dataHeaderRow,
                LOCATION_COLUMN, "Location");
        Utils.writeStringCell(titleCellStyle, dataHeaderRow,
                FIRST_NAME_COLUMN, "First Name");
        Utils.writeStringCell(titleCellStyle, dataHeaderRow,
                LAST_NAME_COLUMN, "Last Name");
        Utils.writeStringCell(titleCellStyle, dataHeaderRow,
                MEDICAL_CONDITION_COLUMN, "Medical Condition");
        Utils.writeStringCell(titleCellStyle, dataHeaderRow,
                MEDICATION_COLUMN, "Medication");
    }

    private void writeCamperAllergies(XSSFSheet allergiesSheet) {
        try {
            // 0 and 1 are header rows
            int currentRow = 2;
            TreeSet<Camper> sortedCampers = new TreeSet<>(campers);
            for (Camper camper : sortedCampers) {
                String allergies = camper.getAllergies().strip().toLowerCase(Locale.ROOT);
                if (!allergies.equals("") && !allergies.equals("no") &&
                        !allergies.equals("none")) {
                    Row row = allergiesSheet.createRow(currentRow);
                    Utils.writeStringCell(defaultCellStyle, row,
                            LOCATION_COLUMN, camper.getLocation());
                    Utils.writeStringCell(defaultCellStyle, row,
                            FIRST_NAME_COLUMN, camper.getFirstName());
                    Utils.writeStringCell(defaultCellStyle, row,
                            LAST_NAME_COLUMN, camper.getLastName());
                    Utils.writeStringCell(defaultCellStyle, row,
                            MEDICAL_CONDITION_COLUMN, allergies);
                    Utils.writeStringCell(defaultCellStyle, row,
                            MEDICATION_COLUMN, "");
                    ++currentRow;
                }
            }
        } catch (Exception e) {
            Utils.showAlertBox(Alert.AlertType.ERROR,
                    "Error",
                    LOG_TAG,
                    "Error while trying to write allergy data to file, " +
                            "unable to save file!");
            Utils.writeStackTrace(e.getStackTrace());
        }
    }
}
