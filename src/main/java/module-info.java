module application.campimpactdataformatter {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.apache.poi.ooxml;

    opens application to javafx.fxml;
    opens model to javafx.fxml;

    exports application;
    exports model;
}