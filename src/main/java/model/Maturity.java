package model;

public enum Maturity {
    ADULT, CHILD;

    private final String LOG_TAG = this.getClass().getName();

    @Override
    public String toString() {
        return switch (this) {
            case ADULT -> "Adult";
            case CHILD -> "Child";
        };
    }

    /**
     * Tries to convert [value] to Maturity enum, if this fails, it assumes
     * adult size
     * @param value - string to convert to enum
     * @return
     */
    public static Maturity fromString(String value) {
        try {
            return valueOf(value.toUpperCase().strip());
        } catch (Exception e) {
            return ADULT;
        }
    }
}
