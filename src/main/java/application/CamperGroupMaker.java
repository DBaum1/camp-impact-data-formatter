package application;

import javafx.scene.control.Alert;
import model.Camper;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class CamperGroupMaker {
    private final HashSet<Camper> campers;
    private int currentGroupNumber = 1;
    private int currentGroupSize = 0;
    private final TreeMap<Double, HashSet<Camper>> campersByAge = new TreeMap<>();
    private final TreeMap<Integer, TreeSet<Camper>> campersByGroupNumber = new TreeMap<>();

    private final String LOG_TAG = this.getClass().getName();

    public CamperGroupMaker(HashSet<Camper> campers, double numGroupsToMake) {
        this.campers = campers;
        createAgeGroups(numGroupsToMake);
    }

    /**
     * Tries to create evenly populated groups of campers
     * (max group size = total campers/numGroups)
     * @param numGroups - number of groups to create
     */
    public void createAgeGroups(double numGroups) {
        mapCampersByAge();
        double maxGroupSize = numGroups > 0 ? Math.ceil(campers.size() / numGroups) : 1;
        Set<Double> ageSet = campersByAge.keySet();
        for (Double camperAge : ageSet) {
            HashSet<Camper> campersInAgeGroup = campersByAge.get(camperAge);
            placeCampersInGroup(campersInAgeGroup, maxGroupSize);
        }
        verifyCamperGroups();
    }

    public TreeMap<Integer, TreeSet<Camper>> getCamperGroups() {
        return campersByGroupNumber;
    }

    /**
     * place each camper per age bucket in a group until the current group is larger than
     * maxGroupSize, if that happens, start a new group
     * Alphabetically ordered by first name
     * @param campersByAge - tree set of campers
     * @param maxGroupSize - max number of campers allowed in per group
     */
    private void placeCampersInGroup(HashSet<Camper> campersByAge, double maxGroupSize) {
        for (Camper camper: campersByAge) {
            if (currentGroupSize == maxGroupSize) {
                currentGroupSize = 0;
                ++currentGroupNumber;
            }
            TreeSet<Camper> camperGroupSet = campersByGroupNumber.get(currentGroupNumber);
            if (camperGroupSet == null) {
                camperGroupSet = new TreeSet<>();
            }
            camperGroupSet.add(camper);
            campersByGroupNumber.put(currentGroupNumber, camperGroupSet);
            ++currentGroupSize;
        }
    }

    /**
     * Maps unique ages to set of all campers with that age
     */
    private void mapCampersByAge() {
        for (Camper camper : campers) {
            Double camperAge = camper.getAge();
            HashSet<Camper> camperAgeSet = campersByAge.get(camperAge);
            if (camperAgeSet == null) {
                HashSet<Camper> newCamperHashSet = new HashSet<>();
                newCamperHashSet.add(camper);
                campersByAge.put(camperAge, newCamperHashSet);
            } else {
                camperAgeSet.add(camper);
            }
        }
    }

    /**
     * Verifies every camper is in a group
     */
    private void verifyCamperGroups() {
        int totalCampers = 0;
        try {
            for (Integer groupNumber : campersByGroupNumber.keySet()) {
                totalCampers += campersByGroupNumber.get(groupNumber).size();
            }
            assert totalCampers == campers.size();
        } catch (Exception e) {
            Utils.showAlertBox(Alert.AlertType.ERROR, "Error", LOG_TAG,
                    "\nError during camper group creation, \nnot all campers have been " +
                            "assigned to a group.");
        }
    }
}
