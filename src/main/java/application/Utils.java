package application;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.DialogPane;
import model.ExcelColumn;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;

/**
 * Provides common utility functions and constants used across the program
 */
public final class Utils {
    public static final String BLANK_STRING_VALUE = "";
    public static final double BLANK_NUMERIC_VALUE = -1.0;
    public static final boolean BLANK_BOOLEAN_VALUE = false;
    public static final Integer KIDS_SHEET_NUMBER = 0;
    public static final String LOCATION_HEADER = "Location";
    public static final String FIRST_NAME_HEADER = "First Name";
    public static final String LAST_NAME_HEADER = "Last Name";
    public static final String SHIRT_SIZE_HEADER = "Shirt Size";
    public static final String SHOE_SIZE_HEADER = "Shoe Size";
    public static final String MATURITY_HEADER = "Adult Child";
    public static final String AGE_HEADER = "Age";
    public static final String GENDER_HEADER = "Gender";
    public static final String MEDIA_RELEASE_HEADER = "Media Release";
    public static final String ALLERGIES_HEADER = "Allergies";
    public static final String SWIM_HEADER = "Swim";
    public static final DataFormatter DATA_FORMATTER = new DataFormatter();
    public static boolean errorEncountered = false;

    private static PrintWriter writer = null;

    static {
        try {
            writer = new PrintWriter(new BufferedWriter(new FileWriter("log.txt", false)));
        } catch (IOException e) {
            Utils.showAlertBox(Alert.AlertType.ERROR,
                    "Error",
                    "Utils",
                    "\nError while trying to write log file");
        }
    }

    private Utils() {
        /* pure utility function class -
           can't be instantiated by client code */
    }

    public static void showAlertBox(AlertType alertType,
                                    String title,
                                    String logTag,
                                    String content) {
        Alert alert = new Alert(alertType);
        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add(
                Utils.class.getResource("CampImpactDataFormatter.css").toExternalForm());

        alert.setTitle(title);
        alert.setContentText(logTag + ": " + content);
        alert.showAndWait();
    }

    public static XSSFCellStyle createCellStyle(XSSFWorkbook workbook,
                                                BorderStyle borderStyle,
                                                HorizontalAlignment horizontalAlignment,
                                                short fontSize,
                                                String fontName,
                                                short color,
                                                boolean wrapText) {
        XSSFCellStyle style = workbook.createCellStyle();
        style.setBorderTop(borderStyle);
        style.setBorderBottom(borderStyle);
        style.setBorderLeft(borderStyle);
        style.setBorderRight(borderStyle);
        style.setAlignment(horizontalAlignment);

        XSSFFont font = workbook.createFont();
        font.setFontHeightInPoints(fontSize);
        font.setFontName(fontName);
        font.setColor(color);

        style.setFont(font);
        style.setWrapText(wrapText);
        return style;
    }

    public static XSSFSheet createNewSheet(XSSFWorkbook workbook, String sheetName) {
        XSSFSheet sheet = workbook.getSheet(sheetName);
        if (sheet != null) {
            int index = workbook.getSheetIndex(sheet);
            workbook.removeSheetAt(index);
        }
        return workbook.createSheet(sheetName);
    }

    public static void writeStringCell(XSSFCellStyle style,
                                       Row row,
                                       ExcelColumn column,
                                       String value) {
        try {
            if (column != null && column.columnNumber() >= 0) {
                Cell cell = row.createCell(column.columnNumber());
                cell.setCellStyle(style);
                cell.setCellValue(value);
            }
        } catch (Exception e) {
            errorEncountered = true;
            writeStackTrace(e.getStackTrace());
        }
    }

    public static void writeStringCell(XSSFCellStyle style,
                                       Row row,
                                       int columnNumber,
                                       String value) {
        try {
            if (columnNumber >= 0) {
                Cell cell = row.createCell(columnNumber);
                cell.setCellStyle(style);
                cell.setCellValue(value);
            }
        } catch (Exception e) {
            errorEncountered = true;
            writeStackTrace(e.getStackTrace());
        }
    }

    public static void writeNumericCell(XSSFCellStyle style,
                                        Row row,
                                        ExcelColumn column,
                                        Double value) {
        try {
            if (column != null && column.columnNumber() != -1) {
                Cell cell = row.createCell(column.columnNumber());
                cell.setCellStyle(style);
                if (value != Utils.BLANK_NUMERIC_VALUE) {
                    cell.setCellValue(value);
                } else {
                    cell.setCellValue("N/A");
                }
            }
        } catch (Exception e) {
            errorEncountered = true;
            writeStackTrace(e.getStackTrace());
        }
    }

    public static void writeNumericCell(XSSFCellStyle style,
                                        Row row,
                                        int columnNumber,
                                        Double value) {
        try {
            if (columnNumber >= 0) {
                Cell cell = row.createCell(columnNumber);
                cell.setCellStyle(style);
                if (value != Utils.BLANK_NUMERIC_VALUE) {
                    cell.setCellValue(value);
                } else {
                    cell.setCellValue("N/A");
                }
            }
        } catch (Exception e) {
            errorEncountered = true;
            writeStackTrace(e.getStackTrace());
        }
    }

    /**
     * @param defaultStyle - how to write a value representing true
     * @param warningStyle - how to write a value representing false
     * @param row - current row to write to, must be >= 0
     * @param column - current column to write to, must be >= 0
     * @param value - value to write
     */
    public static void writeBooleanCellAsString(XSSFCellStyle defaultStyle,
                                          XSSFCellStyle warningStyle,
                                          Row row,
                                          ExcelColumn column,
                                          boolean value) {
        try {
            if (column != null && column.columnNumber() != -1) {
                Cell cell = row.createCell(column.columnNumber());
                if (value) {
                    cell.setCellStyle(defaultStyle);
                    cell.setCellValue("Yes");
                } else {
                    cell.setCellStyle(warningStyle);
                    cell.setCellValue("NO");
                }
            }
        } catch (Exception e) {
            errorEncountered = true;
            writeStackTrace(e.getStackTrace());
        }
    }

    public static String extractStringCell(ExcelColumn excelColumn, Row row) {
        if (excelColumn != null) {
            Cell cell = row.getCell(excelColumn.columnNumber());
            if (isNotBlankCell(cell)) {
                return DATA_FORMATTER.formatCellValue(cell);
            }
        }
        return Utils.BLANK_STRING_VALUE;
    }

    public static Double extractNumericCell(ExcelColumn excelColumn, Row row) {
        if (excelColumn != null) {
            Cell cell = row.getCell(excelColumn.columnNumber());
            if (isNotBlankCell(cell) && isNumericCell(cell)) {
                return cell.getNumericCellValue();
            }
        }
        return Utils.BLANK_NUMERIC_VALUE;
    }

    public static Boolean extractBooleanCell(ExcelColumn excelColumn, Row row) {
        if (excelColumn != null) {
            Cell cell = row.getCell(excelColumn.columnNumber());
            if (isNotBlankCell(cell)) {
                String value = DATA_FORMATTER.formatCellValue(cell).strip().toLowerCase(Locale.ROOT);
                if (value.equals("yes") || value.equals("true") ||
                        value.equals("y") || value.equals("t")) {
                    return true;
                }
            }
        }
        return Utils.BLANK_BOOLEAN_VALUE;
    }

    public static void autoSizeColumns(XSSFSheet sheet, int numColumns) {
        for (int columnNumber = 0; columnNumber < numColumns; ++columnNumber) {
            sheet.autoSizeColumn(columnNumber);
        }
    }

    public static boolean isNumericCell(Cell cell) {
        return cell != null && cell.getCellType() == CellType.NUMERIC;
    }

    public static boolean isNotBlankCell(Cell cell) {
        return cell != null && cell.getCellType() != CellType.BLANK;
    }

    public static void writeStackTrace(StackTraceElement[] stackTraceElement) {
        writer.write(Arrays.toString(stackTraceElement));
        writer.write(System.lineSeparator());
        writer.flush();
    }

    public static void closeWriter() {
        writer.close();
    }
}
