package application;

import model.Camper;
import model.Gender;
import model.Maturity;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.stream.Stream;

public class ShoeQuantityFormatter implements ExcelFileFormatter {
    private final HashSet<Camper> campers;
    private final XSSFWorkbook workbook;
    private final XSSFCellStyle defaultCellStyle;
    XSSFCellStyle titleCellStyle;
    private final String LOG_TAG = this.getClass().getName();

    private static final int SHOE_SIZE_COLUMN = 0;
    private static final int GENDER_COLUMN = 1;
    private static final int SHOE_QUANTITY_COLUMN = 2;
    private static final int COLUMN_OFFSET = 4;    // skip a column between males and females
    private static final int NUM_COLUMNS = 3 + COLUMN_OFFSET;

    private int rowNum = 0;
    private double allShoesTotal = 0;

    public ShoeQuantityFormatter(XSSFWorkbook workbook,
                                 XSSFCellStyle defaultCellStyle,
                                 XSSFCellStyle titleCellStyle,
                                 HashSet<Camper> campers) {
        this.workbook = workbook;
        this.defaultCellStyle = defaultCellStyle;
        this.titleCellStyle = titleCellStyle;
        this.campers = campers;
    }

    @Override
    public void writeToFile() {
        XSSFSheet shoeSheet = Utils.createNewSheet
                (workbook, "Shoe Quantities");
        writeHeaders(shoeSheet);

        Stream<Camper> childFemaleCampers = campers.stream().filter(camper ->
                camper.getGender() == Gender.FEMALE &&
                        camper.getMaturity() == Maturity.CHILD);
        Stream<Camper> adultFemaleCampers = campers.stream().filter(camper ->
                camper.getGender() == Gender.FEMALE &&
                        camper.getMaturity() == Maturity.ADULT);
        Stream<Camper> childMaleCampers = campers.stream().filter(camper ->
                camper.getGender() == Gender.MALE &&
                        camper.getMaturity() == Maturity.CHILD);
        Stream<Camper> adultMaleCampers = campers.stream().filter(camper ->
                camper.getGender() == Gender.MALE &&
                        camper.getMaturity() == Maturity.ADULT);

        // female camper shoe quantities
        writeSubtitles(shoeSheet, "Child", 0);
        writeShoeQuantities(shoeSheet, childFemaleCampers, 0, "F");
        skipRow(2);   // skip rows for aesthetic purposes
        writeSubtitles(shoeSheet, "Adult", 0);
        writeShoeQuantities(shoeSheet, adultFemaleCampers, 0, "F");

        // male camper shoe quantities
        rowNum = 1;   // restart at top under header
        writeSubtitles(shoeSheet, "Child", COLUMN_OFFSET);
        writeShoeQuantities(shoeSheet, childMaleCampers, COLUMN_OFFSET, "M");
        skipRow(2);   // skip rows for aesthetic purposes
        writeSubtitles(shoeSheet, "Adult", COLUMN_OFFSET);
        writeShoeQuantities(shoeSheet, adultMaleCampers, COLUMN_OFFSET, "M");
        Utils.autoSizeColumns(shoeSheet, NUM_COLUMNS);

        skipRow(2);   // skip rows for aesthetic purposes
        // write overall shoe total
        Row shoeTotalRow = getNextRow(shoeSheet);
        Utils.writeStringCell(titleCellStyle, shoeTotalRow,
                GENDER_COLUMN + COLUMN_OFFSET, "All Shoes Total");
        Utils.writeNumericCell(titleCellStyle, shoeTotalRow,
                SHOE_QUANTITY_COLUMN + COLUMN_OFFSET, allShoesTotal);
    }

    private void writeHeaders(XSSFSheet shoeSheet) {
        Row titleRow = getNextRow(shoeSheet);
        Utils.writeStringCell(titleCellStyle, titleRow,
                SHOE_SIZE_COLUMN, "Shoe Size");
        Utils.writeStringCell(titleCellStyle, titleRow,
                GENDER_COLUMN, "Gender");
        Utils.writeStringCell(titleCellStyle, titleRow,
                SHOE_QUANTITY_COLUMN, "Quantity");

        Utils.writeStringCell(titleCellStyle, titleRow,
                SHOE_SIZE_COLUMN + COLUMN_OFFSET, "Shoe Size");
        Utils.writeStringCell(titleCellStyle, titleRow,
                GENDER_COLUMN + COLUMN_OFFSET, "Gender");
        Utils.writeStringCell(titleCellStyle, titleRow,
                SHOE_QUANTITY_COLUMN + COLUMN_OFFSET, "Quantity");
        skipRow(1);
    }

    private void writeShoeQuantities(XSSFSheet shoeSheet,
                                     Stream<Camper> filteredCampers,
                                     int columnOffset,
                                     String gender) {
        TreeMap<Double, Integer> shoeQuantities = getShoeQuantities(filteredCampers);
        double total = 0;
        for (Double shoeSize : shoeQuantities.keySet()) {
            Row row = getNextRow(shoeSheet);
            Utils.writeNumericCell(defaultCellStyle, row,
                    SHOE_SIZE_COLUMN + columnOffset, shoeSize);
            Utils.writeStringCell(defaultCellStyle, row,
                    GENDER_COLUMN + columnOffset, gender);

            double quantity = shoeQuantities.get(shoeSize);
            total += quantity;
            Utils.writeNumericCell(defaultCellStyle, row,
                    SHOE_QUANTITY_COLUMN + columnOffset, quantity);
            ++rowNum;
        }
        Row totalRow = getNextRow(shoeSheet);
        Utils.writeStringCell(titleCellStyle, totalRow,
                GENDER_COLUMN + columnOffset, "Total");
        Utils.writeNumericCell(titleCellStyle, totalRow,
                SHOE_QUANTITY_COLUMN + columnOffset, total);
        allShoesTotal += total;
    }

    private TreeMap<Double, Integer> getShoeQuantities(Stream<Camper> filteredCampers) {
        TreeMap<Double, Integer> shoeQuantities = new TreeMap<>();
        filteredCampers.forEach(camper -> {
            double shoeSize = camper.getShoeSize();
            shoeQuantities.merge(shoeSize, 1, Integer::sum);
        });
        return shoeQuantities;
    }

    /**
     * Writes 'Adult' or 'Child' for shoe size subdivisions
     */
    private void writeSubtitles(XSSFSheet shoeSheet,
                                String value,
                                int offset) {
        Row row = getNextRow(shoeSheet);
        Utils.writeStringCell(titleCellStyle, row,
                SHOE_SIZE_COLUMN + offset, value);
        shoeSheet.addMergedRegion(
                new CellRangeAddress(rowNum, rowNum,
                        SHOE_SIZE_COLUMN + offset,
                        SHOE_QUANTITY_COLUMN + offset));
        ++rowNum;
    }

    private void skipRow(int rowsToSkip) {
        rowNum += rowsToSkip;
    }

    private Row getNextRow(XSSFSheet shoeSheet) {
        return shoeSheet.getRow(rowNum) == null ? shoeSheet.createRow(rowNum) : shoeSheet.getRow(rowNum);
    }
}
