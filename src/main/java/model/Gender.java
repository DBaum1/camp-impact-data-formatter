package model;

public enum Gender {
    MALE, FEMALE, INDETERMINATE;

    private final String LOG_TAG = this.getClass().getName();

    @Override
    public String toString() {
        return switch (this) {
            case MALE -> "M";
            case FEMALE -> "F";
            default -> "";
        };
    }

    public static Gender fromString(String value) {
        try {
            return switch (value.strip().toLowerCase()) {
                case "male", "m", "boy", "man" -> MALE;
                case "female", "f", "girl", "woman" -> FEMALE;
                default -> INDETERMINATE;
            };
        } catch (Exception e) {
            return INDETERMINATE;
        }
    }
}
