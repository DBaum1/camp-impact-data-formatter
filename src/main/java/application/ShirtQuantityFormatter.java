package application;

import model.Camper;
import model.ShirtSize;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.EnumMap;
import java.util.HashSet;

/**
 * Writes Shirt sizes and quantity
 */
public class ShirtQuantityFormatter implements ExcelFileFormatter {
    private final HashSet<Camper> campers;
    private final XSSFWorkbook workbook;
    private final XSSFCellStyle defaultCellStyle;
    private final XSSFCellStyle titleCellStyle;
    private final String LOG_TAG = this.getClass().getName();

    private static final int NUM_COLUMNS = 2;
    private static final int SHIRT_SIZE_COLUMN = 0;
    private static final int SHIRT_QUANTITY_COLUMN = 1;

    public ShirtQuantityFormatter(XSSFWorkbook workbook,
                                  XSSFCellStyle defaultCellStyle,
                                  XSSFCellStyle titleCellStyle,
                                  HashSet<Camper> campers) {
        this.workbook = workbook;
        this.defaultCellStyle = defaultCellStyle;
        this.titleCellStyle = titleCellStyle;
        this.campers = campers;
    }

    @Override
    public void writeToFile() {
        XSSFSheet shirtSheet = Utils.createNewSheet
                (workbook, "Shirt Quantity");
        writeHeaders(shirtSheet);
        writeShirtQuantities(shirtSheet);
        Utils.autoSizeColumns(shirtSheet, NUM_COLUMNS);
    }

    private void writeHeaders(XSSFSheet shirtSheet) {
        Row titleRow = shirtSheet.createRow(0);
        Utils.writeStringCell(titleCellStyle, titleRow,
                SHIRT_SIZE_COLUMN, "Shirt Size");
        Utils.writeStringCell(titleCellStyle, titleRow,
                SHIRT_QUANTITY_COLUMN, "Quantity");
    }

    private void writeShirtQuantities(XSSFSheet shirtSheet) {
        // row 0 is for headers
        int rowNum = 1;
        double totalShirts = 0;
        EnumMap<ShirtSize, Integer> shirtQuantities = getShirtQuantities();
        for (ShirtSize shirtSize : shirtQuantities.keySet()) {
            Row row = shirtSheet.createRow(rowNum);
            Utils.writeStringCell(defaultCellStyle, row,
                    SHIRT_SIZE_COLUMN, shirtSize.toString());

            double quantity = shirtQuantities.get(shirtSize);
            totalShirts += quantity;

            Utils.writeNumericCell(defaultCellStyle, row,
                    SHIRT_QUANTITY_COLUMN, quantity);
            ++rowNum;
        }
        Row endRow = shirtSheet.createRow(rowNum);
        Utils.writeStringCell(defaultCellStyle, endRow,
                SHIRT_SIZE_COLUMN, "Total");
        Utils.writeNumericCell(defaultCellStyle, endRow,
                SHIRT_QUANTITY_COLUMN, totalShirts);
    }

    private EnumMap<ShirtSize, Integer> getShirtQuantities() {
        EnumMap<ShirtSize, Integer> shirtQuantities = new EnumMap<>(ShirtSize.class);
        for (Camper camper : campers) {
            ShirtSize shirtSize = (ShirtSize)camper.getShirtSize();
            shirtQuantities.merge(shirtSize, 1, Integer::sum);
        }
        return shirtQuantities;
    }
}
