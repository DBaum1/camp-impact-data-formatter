package model;

public enum ShirtSize {
    YXS, YS, YM, YL, YXL, AXS, AS, AM, AL, AXL, INDETERMINATE;

    private final String LOG_TAG = this.getClass().getName();

    @Override
    public String toString() {
        if (this == INDETERMINATE) {
            return "";
        } else {
            return super.toString();
        }
    }

    public static ShirtSize fromString(String value) {
        try {
            return valueOf(value.toUpperCase().strip());
        } catch (Exception e) {
            return INDETERMINATE;
        }
    }
}
