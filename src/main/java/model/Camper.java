package model;

public class Camper implements Comparable<Camper> {
    private final String location;
    private final String firstName;
    private final String lastName;
    private final Enum<ShirtSize> shirtSize;
    private final Double shoeSize;
    private final Enum<Maturity> maturity;
    private final Double age;
    private final Enum<Gender> gender;
    private final Boolean mediaRelease;
    private final String allergies;
    private final Boolean canSwim;

    private Camper(CamperBuilder builder) {
        this.location = builder.location;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.shirtSize = builder.shirtSize;
        this.shoeSize = builder.shoeSize;
        this.maturity = builder.maturity;
        this.age = builder.age;
        this.gender = builder.gender;
        this.mediaRelease = builder.mediaRelease;
        this.allergies = builder.allergies;
        this.canSwim = builder.canSwim;
    }

    public String getLocation() {
        return location;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Enum<ShirtSize> getShirtSize() {
        return shirtSize;
    }

    public Double getShoeSize() {
        return shoeSize;
    }

    public Enum<Maturity> getMaturity() { return maturity; }

    public double getAge() {
        return age;
    }

    public Enum<Gender> getGender() {
        return gender;
    }

    public Boolean getMediaRelease() {
        return mediaRelease;
    }

    public String getAllergies() {
        return allergies;
    }

    public Boolean getCanSwim() {
        return canSwim;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result += location == null ? 0 : location.hashCode();
        result += firstName == null ? 0 : firstName.hashCode();
        result += lastName == null ? 0 : lastName.hashCode();
        result += shirtSize == null ? 0 : shirtSize.hashCode();
        result += shoeSize == null ? 0 : shoeSize.hashCode();
        result += maturity == null ? 0 : maturity.hashCode();
        result += age == null ? 0 : age.hashCode();
        result += gender == null ? 0 : gender.hashCode();
        result += mediaRelease == null ? 0 : mediaRelease.hashCode();
        result += allergies == null ? 0 : allergies.hashCode();
        result += canSwim == null ? 0 : canSwim.hashCode();
        result = prime * result;
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Camper)) {
            return false;
        }

        Camper otherCamper = (Camper) o;
        return this.location.equals(otherCamper.location) &&
                this.firstName.equals(otherCamper.firstName) &&
                this.lastName.equals(otherCamper.lastName) &&
                this.shirtSize.equals(otherCamper.shirtSize) &&
                this.shoeSize.equals(otherCamper.shoeSize) &&
                this.maturity.equals(otherCamper.maturity) &&
                this.age.equals(otherCamper.age) &&
                this.gender.equals(otherCamper.gender) &&
                this.mediaRelease == otherCamper.mediaRelease &&
                this.allergies.equals(otherCamper.allergies) &&
                this.canSwim == otherCamper.canSwim;
    }

    public String toString() {
        return "Location: " + location +
                "\nFirst name: " + firstName +
                "\nLast name: " + lastName +
                "\nShirt size: " + shirtSize +
                "\nShoe size: " + shoeSize +
                "\nMaturity:" + maturity +
                "\nAge: " + age +
                "\nGender: " + gender +
                "\nMedia Release: " + mediaRelease +
                "\nAllergies: " + allergies +
                "\nCan Swim: " + canSwim;
    }

    // sorts in alphabetical order of first name
    @Override
    public int compareTo(Camper o) {
        if (firstName == null || o.getFirstName() == null) {
            return 0;
        }
        return firstName.compareTo(o.getFirstName());
    }

    public static class CamperBuilder {
        private String location;
        private String firstName;
        private String lastName;
        private Enum<ShirtSize> shirtSize;
        private Double shoeSize;
        private Enum<Maturity> maturity;
        private Double age;
        private Enum<Gender> gender;
        private Boolean mediaRelease;
        private String allergies;
        private Boolean canSwim;

        public CamperBuilder() { }

        public CamperBuilder location(String location) {
            this.location = location;
            return this;
        }

        public CamperBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public CamperBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public CamperBuilder shirtSize(Enum<ShirtSize> shirtSize) {
            this.shirtSize = shirtSize;
            return this;
        }

        public CamperBuilder shoeSize(Double shoeSize) {
            this.shoeSize = shoeSize;
            return this;
        }

        public CamperBuilder maturity(Enum<Maturity> maturity) {
            this.maturity = maturity;
            return this;
        }

        public CamperBuilder age(Double age) {
            this.age = age;
            return this;
        }

        public CamperBuilder gender(Enum<Gender> gender) {
            this.gender = gender;
            return this;
        }

        public CamperBuilder mediaRelease(Boolean mediaRelease) {
            this.mediaRelease = mediaRelease;
            return this;
        }

        public CamperBuilder allergies(String allergies) {
            this.allergies = allergies;
            return this;
        }

        public CamperBuilder canSwim(Boolean canSwim) {
            this.canSwim = canSwim;
            return this;
        }

        public Camper build() {
            return new Camper(this);
        }
    }
}
