package application;

import javafx.scene.control.Alert;
import model.Camper;
import model.ExcelColumn;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.HashMap;
import java.util.TreeMap;
import java.util.TreeSet;

public class CamperGroupFormatter implements ExcelFileFormatter {
    private final String LOG_TAG = this.getClass().getName();
    private final XSSFWorkbook workbook;
    private final TreeMap<Integer, TreeSet<Camper>> camperGroups;
    private final XSSFCellStyle defaultCellStyle;
    private final XSSFCellStyle warningCellStyle;
    private final XSSFCellStyle titleCellStyle;
    private final HashMap<String, ExcelColumn> columns;

    public CamperGroupFormatter(XSSFWorkbook workbook,
                                HashMap<String, ExcelColumn> columns,
                                XSSFCellStyle defaultCellStyle,
                                XSSFCellStyle warningCellStyle,
                                XSSFCellStyle titleCellStyle,
                                TreeMap<Integer, TreeSet<Camper>> camperGroups) {
        this.workbook = workbook;
        this.columns = columns;
        this.camperGroups = camperGroups;
        this.defaultCellStyle = defaultCellStyle;
        this.warningCellStyle = warningCellStyle;
        this.titleCellStyle = titleCellStyle;
    }

    @Override
    public void writeToFile() {
        try {
            XSSFSheet groupsSheet = Utils.createNewSheet
                    (workbook, "Print Groups");
            int currentRow = 0;
            for (Integer groupNumber : camperGroups.keySet()) {
                Row groupHeaderRow = groupsSheet.createRow(currentRow);
                writeGroupHeader(groupsSheet, groupHeaderRow,
                        groupNumber);
                ++currentRow;
                writeCamperDataHeader(groupsSheet, currentRow);
                ++currentRow;
                TreeSet<Camper> campersInGroup = new TreeSet<>(camperGroups.get(groupNumber));
                for (Camper camper : campersInGroup) {
                    Row camperRow = groupsSheet.createRow(currentRow);
                    writeCamperData(camperRow, camper);
                    ++currentRow;
                }
            }
        } catch (Exception e) {
            Utils.showAlertBox(Alert.AlertType.ERROR,
                    "Error",
                    LOG_TAG,
                    "Error while trying to write groups to file, " +
                            "unable to save file!");
            Utils.writeStackTrace(e.getStackTrace());
        }
    }

    /**
     * Writes group number in form Group [group num]
     * newline that's the width of the number of camper data columns
     * @param groupsSheet - current sheet we're writing
     * @param row - current row being written to
     * @param groupNumber - current group number we're writing
     */
    private void writeGroupHeader(XSSFSheet groupsSheet,
                                  Row row,
                                  int groupNumber) {
        int rowNum = row.getRowNum();
        Cell headerCell = row.createCell(0);
        headerCell.setCellValue("GROUP " + groupNumber + "\n");
        headerCell.setCellStyle(titleCellStyle);
        groupsSheet.addMergedRegion(
                new CellRangeAddress(rowNum, rowNum, 0, getRightmostColumnNumber()));
    }

    private void writeCamperData(Row row,
                                 Camper camper) {
        ExcelColumn locationColumn = columns.get(Utils.LOCATION_HEADER);
        Utils.writeStringCell(defaultCellStyle, row, locationColumn, camper.getLocation());

        ExcelColumn firstNameColumn = columns.get(Utils.FIRST_NAME_HEADER);
        Utils.writeStringCell(defaultCellStyle, row, firstNameColumn,
                camper.getFirstName());

        ExcelColumn lastNameColumn = columns.get(Utils.LAST_NAME_HEADER);
        Utils.writeStringCell(defaultCellStyle, row, lastNameColumn,
                camper.getLastName());

        ExcelColumn shirtSizeColumn = columns.get(Utils.SHIRT_SIZE_HEADER);
        Utils.writeStringCell(defaultCellStyle, row, shirtSizeColumn,
                camper.getShirtSize().toString());

        ExcelColumn shoeSizeColumn = columns.get(Utils.SHOE_SIZE_HEADER);
        Utils.writeNumericCell(defaultCellStyle, row, shoeSizeColumn,
                camper.getShoeSize());

        ExcelColumn maturityColumn = columns.get(Utils.MATURITY_HEADER);
        Utils.writeStringCell(defaultCellStyle, row, maturityColumn,
                camper.getMaturity().toString());

        ExcelColumn ageColumn = columns.get(Utils.AGE_HEADER);
        Utils.writeNumericCell(defaultCellStyle, row, ageColumn,
                camper.getAge());

        ExcelColumn genderColumn = columns.get(Utils.GENDER_HEADER);
        Utils.writeStringCell(defaultCellStyle, row, genderColumn,
                camper.getGender().toString());

        ExcelColumn mediaReleaseColumn = columns.get(Utils.MEDIA_RELEASE_HEADER);
        Utils.writeBooleanCellAsString(defaultCellStyle, warningCellStyle, row, mediaReleaseColumn,
                camper.getMediaRelease());

        ExcelColumn allergiesColumn = columns.get(Utils.ALLERGIES_HEADER);
        Utils.writeStringCell(defaultCellStyle, row, allergiesColumn,
                camper.getAllergies());

        ExcelColumn canSwimColumn = columns.get(Utils.SWIM_HEADER);
        Utils.writeBooleanCellAsString(defaultCellStyle, warningCellStyle, row, canSwimColumn,
                camper.getCanSwim());
    }

    /**
     * Writes column headers for each group's campers
     * @param groupsSheet - current sheet for writing group data
     * @param rowNum - current row to be written
     */
    private void writeCamperDataHeader(XSSFSheet groupsSheet,
                                       int rowNum) {
        try {
            Row row = groupsSheet.createRow(rowNum);
            for (String columnHeader : columns.keySet()) {
                ExcelColumn column = columns.get(columnHeader);
                Utils.writeStringCell(getSubtitleCellStyle(), row, column, column.columnHeader());
                groupsSheet.autoSizeColumn(column.columnNumber());
            }
        } catch (Exception e) {
            Utils.showAlertBox(Alert.AlertType.ERROR,
                    "Error",
                    LOG_TAG,
                    "Error while trying to write group headers to file, " +
                            "unable to save file!");
            Utils.writeStackTrace(e.getStackTrace());
        }
    }

    /**
     * @return rightmost column number
     */
    private int getRightmostColumnNumber() {
        int rightMostColumn = 0;
        for (String columnHeader : columns.keySet()) {
            int columnNumber = columns.get(columnHeader).columnNumber();
            if (columnNumber > rightMostColumn) {
                rightMostColumn = columnNumber;
            }
        }
        return rightMostColumn;
    }

    private XSSFCellStyle getSubtitleCellStyle() {
        return Utils.createCellStyle(workbook,
                BorderStyle.MEDIUM,
                HorizontalAlignment.CENTER,
                (short)11,
                "Calibri",
                Font.COLOR_NORMAL,
                true);
    }
}
