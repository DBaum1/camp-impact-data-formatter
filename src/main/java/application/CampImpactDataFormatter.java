package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class CampImpactDataFormatter extends Application {
    private Stage primaryStage;

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(CampImpactDataFormatter.class.getResource("CampImpactDataFormatter.fxml"));
        Scene scene = new Scene(loader.load(), 600, 700);
        scene.getStylesheets().add(getClass().getResource("CampImpactDataFormatter.css").toExternalForm());
        stage.setTitle("Camp Impact Data Formatter");
        stage.setScene(scene);

        CampImpactDataFormatterController formatterController = loader.getController();
        formatterController.setDataFormatterApp(this);
        this.primaryStage = stage;
        stage.setOnCloseRequest(e -> {
            e.consume();
            formatterController.onQuit();
        });
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }
}