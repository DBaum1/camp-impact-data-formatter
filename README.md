# Installation

Note: Only Windows is currently supported

Download and install Java: https://www.oracle.com/java/technologies/downloads/#jdk19-windows

The x64 Installer is probably the easiest option.

Download the [executable JAR](https://gitlab.com/DBaum1/camp-impact-data-formatter/-/blob/master/CampImpactDataFormatter-1.0-Windows.jar)
and you can now run the program.

# Instructions

Select an Excel file that contains the camper data.

Note: The first row of the first sheet of the Excel
file must contain the following named columns to 
work properly (no duplicates):

* Location
* First Name
* Last Name
* Shirt Size
* Shoe Size
* Adult Child
* Age
* Gender
* Media Release
* Allergies
* Swim

| Title        | Description                                    |
| ------------ | ---------------------------------------------- |
| Location     | Camper's dropoff location                      |
| First Name   | Camper's first name                            |
| Last Name    | Camper's last name                             |
| Shirt Size   | Camper's shirt size (YXS, YS, YM, YL, YXL, AXS, AS, AM, AL, AXL) |
| Shoe Size    | Camper's shoe size (5, 8.5, etc.)              |
| Adult Child  | Camper's youth or adult clothing size (child/adult) |
| Age          | Camper's age                                   |
| Gender       | Camper's gender (male/female/M/F)              |
| Media Release| Camper's media release permission (Y/N/yes/no) |
| Allergies    | Camper's allergies/medical issues (can be left blank) |
| Swim         | Camper's ability to swim (Y/N/yes/no/y/n)      |

# Example data

![Example data](ex.PNG)
Notice the column names match exactly and the camper
data is in the first row of the first sheet.

# Warning

The column titles must match precisely (spelling and
punctuation). Capitalization doesn't matter. 
It's assumed that all camper data is on
the first sheet of the Excel spreadsheet. The program
can still function without some of the data but the
results won't be complete.

# Results

Four extra sheets will be added with the formatted data.
The second sheet contains the created groups, the
third sheet contains campers with allergies, the 
fourth tallies the shirt quantity, the fifth
tallies the shoe quantity. As always, you should
manually review the results to be sure they're what
you want.

# Questions and Error Reporting

In the directory you downloaded this to, there is a
log file that will be written to whenever something
goes wrong. Feel free to contact me and send the log
file. The log file won't contain personal data.

# License
The Unlicense