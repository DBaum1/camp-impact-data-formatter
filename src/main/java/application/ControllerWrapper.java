package application;

/**
 * Necessary for sharded JAR
 */
public class ControllerWrapper {
    public static void main(String[] args) {
        CampImpactDataFormatter.main(args);
    }
}
