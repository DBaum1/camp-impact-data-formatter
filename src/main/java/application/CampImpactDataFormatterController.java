package application;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import model.Camper;
import model.ExcelColumn;
import model.Gender;
import model.Maturity;
import model.ShirtSize;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Objects;
import java.util.TreeMap;
import java.util.TreeSet;

public class CampImpactDataFormatterController {
    @FXML VBox rootVBox;
    @FXML Button openFileButton;
    @FXML Label openFileLabel;
    @FXML Button formatDataButton;
    @FXML ChoiceBox<Integer> numGroupsChoiceBox;
    @FXML Label numGroupsLabel;
    @FXML Button saveFileButton;
    @FXML Label savedFileLabel;
    @FXML Label columnsReadLabel;
    @FXML Label tableCheckLabel;
    @FXML TableView<ExcelColumn> columnHeaderTable;
    @FXML TableColumn<ExcelColumn, String> columnNameCol;
    @FXML TableColumn<ExcelColumn, Integer> columnNumberCol;
    @FXML ScrollPane scrollPane;

    private final FileChooser fileChooser;
    private CampImpactDataFormatter dataFormatterApp;
    private File currentOpenFile = null;
    private final String LOG_TAG = this.getClass().getName();
    private XSSFWorkbook workbook = null;
    private final Camper.CamperBuilder camperBuilder = new Camper.CamperBuilder();

    ObservableList<Integer> numGroupsList = FXCollections.observableArrayList(
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
    ObservableList<ExcelColumn> columnHeaderTableList = FXCollections.observableArrayList();

    public CampImpactDataFormatterController() {
        fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Excel (*.xlsx)", "*.xlsx"));
    }

    @FXML
    private void initialize() {
        setupColumnTable();
        numGroupsChoiceBox.setValue(5);
        numGroupsChoiceBox.setItems(numGroupsList);
        columnsReadLabel.setText("Note: The first row of the first sheet of the Excel file must contain " +
                "the following \nnamed columns to work properly (no duplicates):\n" +
                Utils.LOCATION_HEADER + "\n" +
                Utils.FIRST_NAME_HEADER + "\n" +
                Utils.LAST_NAME_HEADER + "\n" +
                Utils.SHIRT_SIZE_HEADER + "\n" +
                Utils.SHOE_SIZE_HEADER + "\n" +
                Utils.MATURITY_HEADER + "\n" +
                Utils.AGE_HEADER + "\n" +
                Utils.GENDER_HEADER + "\n" +
                Utils.MEDIA_RELEASE_HEADER + "\n" +
                Utils.ALLERGIES_HEADER + "\n" +
                Utils.SWIM_HEADER);
    }

    @FXML
    private void onOpen() {
        fileChooser.setTitle("Open Excel File");
        File selectedExcelFile = fileChooser.showOpenDialog(dataFormatterApp.getPrimaryStage());
        if (selectedExcelFile != null && selectedExcelFile.isFile() && selectedExcelFile.exists()) {
            try {
                FileInputStream fileInputStream = new FileInputStream(selectedExcelFile);
                workbook = new XSSFWorkbook(fileInputStream);
                currentOpenFile = selectedExcelFile;
                openFileLabel.setText("Current open file: " + currentOpenFile);
                File parentFile = selectedExcelFile.getParentFile();
                fileChooser.setInitialDirectory(parentFile);
            } catch (IOException e) {
                Utils.showAlertBox(Alert.AlertType.ERROR,
                        "Error",
                        LOG_TAG,
                        "\nError while trying to open file, check its formatting");
                Utils.writeStackTrace(e.getStackTrace());
            }
        }
    }

    @FXML
    private void onFormatData() {
        if (currentOpenFile != null && workbook != null) {
            XSSFSheet kidsDataSpreadsheet = workbook.getSheetAt(Utils.KIDS_SHEET_NUMBER);
            HashMap<String, ExcelColumn> columns = readColumnHeaders(kidsDataSpreadsheet);
            HashSet<Camper> campers = generateCamperSet(kidsDataSpreadsheet, columns);
            XSSFCellStyle defaultCellStyle = getDefaultCellStyle();
            XSSFCellStyle warningCellStyle = getWarningCellStyle();
            XSSFCellStyle titleCellStyle = getTitleCellStyle();
            formatGroupData(columns, campers, defaultCellStyle, warningCellStyle, titleCellStyle);
            formatAllergyData(new TreeSet<>(campers), defaultCellStyle, titleCellStyle);
            formatShirtData(campers, defaultCellStyle, titleCellStyle);
            formatShoeData(campers, defaultCellStyle, titleCellStyle);
            checkForFormattingError();
        } else {
            Utils.showAlertBox(Alert.AlertType.INFORMATION, "Information", LOG_TAG,
                    "\nYou never opened a file - no data to format!");
        }
    }

    private void checkForFormattingError() {
        if (Utils.errorEncountered) {
            Utils.showAlertBox(Alert.AlertType.ERROR,
                    "Error",
                    LOG_TAG,
                    "\nError while trying to write data to file, unable to save file!" +
                            "\nCheck log.txt at " + System.getProperty("user.dir") +
                            "\nfor more info on error");
            Utils.errorEncountered = false;
        }
    }

    private void formatGroupData(HashMap<String, ExcelColumn> columns,
                                 HashSet<Camper> campers,
                                 XSSFCellStyle defaultCellStyle,
                                 XSSFCellStyle warningCellStyle,
                                 XSSFCellStyle titleCellStyle) {
        CamperGroupMaker camperGroupMaker = new CamperGroupMaker
                (campers, numGroupsChoiceBox.getValue());
        TreeMap<Integer, TreeSet<Camper>> camperGroups = camperGroupMaker.getCamperGroups();
        CamperGroupFormatter camperGroupFormatter = new CamperGroupFormatter(workbook,
                columns, defaultCellStyle, warningCellStyle, titleCellStyle, camperGroups);
        camperGroupFormatter.writeToFile();
    }

    private void formatAllergyData(TreeSet<Camper> campers,
                                   XSSFCellStyle defaultCellStyle,
                                   XSSFCellStyle titleCellStyle) {
        AllergyDataFormatter camperAllergyFormatter = new AllergyDataFormatter(workbook,
                defaultCellStyle, titleCellStyle, campers);
        camperAllergyFormatter.writeToFile();
    }

    private void formatShirtData(HashSet<Camper> campers,
                                 XSSFCellStyle defaultCellStyle,
                                 XSSFCellStyle titleCellStyle) {
        ShirtQuantityFormatter camperShirtFormatter = new ShirtQuantityFormatter(workbook,
                defaultCellStyle, titleCellStyle, campers);
        camperShirtFormatter.writeToFile();
    }

    private void formatShoeData(HashSet<Camper> campers,
                                XSSFCellStyle defaultCellStyle,
                                XSSFCellStyle titleCellStyle) {
        ShoeQuantityFormatter shoeQuantityFormatter = new ShoeQuantityFormatter(workbook,
                defaultCellStyle, titleCellStyle, campers);
        shoeQuantityFormatter.writeToFile();
    }

    @FXML
    private void onSave() {
        if (currentOpenFile != null) {
            fileChooser.setTitle("Save to Excel File");
            String defaultSaveName = getDefaultSaveName();
            fileChooser.setInitialFileName(defaultSaveName);
            File selectedSaveFile = fileChooser.showSaveDialog(dataFormatterApp.getPrimaryStage());
            if (selectedSaveFile != null) {
                try {
                    savedFileLabel.setText("Save file: " + selectedSaveFile);
                    File parentFile = selectedSaveFile.getParentFile();
                    fileChooser.setInitialDirectory(parentFile);

                    // actually save formatted data
                    FileOutputStream fileOut = new FileOutputStream(selectedSaveFile);
                    workbook.write(fileOut);
                    fileOut.close();

                    Utils.showAlertBox(Alert.AlertType.CONFIRMATION,
                            "Success",
                            LOG_TAG,
                            "\nSuccessfully saved file!");
                } catch (Exception e) {
                    Utils.showAlertBox(Alert.AlertType.ERROR,
                            "Error",
                            LOG_TAG,
                            "\nError while trying to save file!");
                    Utils.writeStackTrace(e.getStackTrace());
                }
            }
        } else {
            Utils.showAlertBox(Alert.AlertType.INFORMATION,
                    "Information",
                    LOG_TAG,
                    "\nYou never opened a file - nothing to save!");
        }
    }

    @FXML
    public void onQuit() {
        Utils.closeWriter();
        Platform.exit();
    }

    /**
     * Is called by the main application to give a reference back to itself.
     *
     * @param dataFormatterApp - main app for camper data formatting
     */
    public void setDataFormatterApp(CampImpactDataFormatter dataFormatterApp) {
        this.dataFormatterApp = dataFormatterApp;
    }


    /**
     * @return String in YYYY-MM-DD-Camper-Groups form
     */
    private String getDefaultSaveName() {
        LocalDate currentDate = LocalDate.now();
        int year = currentDate.getYear();
        int month = currentDate.getMonthValue();
        int day = currentDate.getDayOfMonth();
        return year + "-" + month + "-" + day + "-Camper-Groups";
    }

    /**
     * @param spreadsheet - current spreadsheet being read
     * @param columns - set of columns
     * @return hash set of all valid campers read from first sheet in workbook
     */
    private HashSet<Camper> generateCamperSet(XSSFSheet spreadsheet,
                                              HashMap<String, ExcelColumn> columns) {
        HashSet<Camper> campers = new HashSet<>();
        readKidsDataSheet(spreadsheet, columns, campers);
        return campers;
    }

    /**
     * Reads the first (index = 0) spreadsheet, which is always assumed to be the
     * sheet with the kids (camper) data
     * @param spreadsheet - current spreadsheet being read
     * @param columns - set of columns
     * @param campers - hash set to add each new camper to
     */
    private void readKidsDataSheet(XSSFSheet spreadsheet,
                                   HashMap<String, ExcelColumn> columns,
                                   HashSet<Camper> campers) {
        for (Row row : spreadsheet) {
            addCamper(row, columns, campers);
        }
    }

    /**
     * Reads the first (index = 0) row of the given spreadsheet to generate list of columns
     * and their headers
     * @param spreadsheet - current spreadsheet being read
     * @return hash map of all columns read from first sheet of first row in workbook
     */
    private HashMap<String, ExcelColumn> readColumnHeaders(XSSFSheet spreadsheet) {
        HashMap<String, ExcelColumn> columns = new HashMap<>();
        Row headerRow = spreadsheet.getRow(0);
        columnHeaderTableList.clear();
        for (Cell cell : headerRow) {
            if (cell.getCellType() != CellType.BLANK) {
                String columnHeader = formatColumnHeader(cell.getStringCellValue());
                Integer columnNumber = cell.getAddress().getColumn();
                ExcelColumn column = new ExcelColumn(columnHeader, columnNumber);
                // don't allow duplicates
                if (!columns.containsKey(column.columnHeader())) {
                    columns.put(columnHeader, column);
                    columnHeaderTableList.add(column);
                }
            }
        }
        columnHeaderTable.setItems(columnHeaderTableList);
        return columns;
    }

    /**
     * Capitalizes the first character of each white space delineated token
     * @param rawHeader - original header read in
     * @return original header with first character of each white space delineated token capitalized and
     * trailing white space stripped
     */
    private String formatColumnHeader(String rawHeader) {
        String strippedHeader = rawHeader.toLowerCase(Locale.ROOT).strip();
        StringBuilder builder = new StringBuilder();
        String[] result = strippedHeader.split("\\s");
        for (String s : result) {
            String firstChar = String.valueOf(s.charAt(0)).toUpperCase(Locale.ROOT);
            if (s.length() > 1) {
                builder.append(firstChar).append(s.substring(1));
            }
            builder.append(" ");
        }
        return builder.toString().stripTrailing();
    }

    /**
     * Extract camper data from the spreadsheet and add to hash set of Campers
     * @param row - row currently being read
     * @param columns - set of columns
     * @param campers - hash set to add each new camper to
     */
    private void addCamper(Row row, HashMap<String, ExcelColumn> columns,
                           HashSet<Camper> campers) {
        ExcelColumn locationColumn = columns.get(Utils.LOCATION_HEADER);
        String location = Utils.extractStringCell(locationColumn, row);
        ExcelColumn firstNameColumn = columns.get(Utils.FIRST_NAME_HEADER);
        String firstName = Utils.extractStringCell(firstNameColumn, row);
        ExcelColumn lastNameColumn = columns.get(Utils.LAST_NAME_HEADER);
        String lastName = Utils.extractStringCell(lastNameColumn, row);

        ExcelColumn shirtSizeColumn = columns.get(Utils.SHIRT_SIZE_HEADER);
        ShirtSize shirtSize = ShirtSize.fromString(Utils.extractStringCell(shirtSizeColumn, row));
        ExcelColumn shoeSizeColumn = columns.get(Utils.SHOE_SIZE_HEADER);
        Double shoeSize = Utils.extractNumericCell(shoeSizeColumn, row);
        ExcelColumn maturityColumn = columns.get(Utils.MATURITY_HEADER);
        Maturity maturity = Maturity.fromString(Utils.extractStringCell(maturityColumn, row));

        ExcelColumn ageColumn = columns.get(Utils.AGE_HEADER);
        Double age = Utils.extractNumericCell(ageColumn, row);

        ExcelColumn genderColumn = columns.get(Utils.GENDER_HEADER);
        Gender gender = Gender.fromString(Utils.extractStringCell(genderColumn, row));

        ExcelColumn mediaReleaseColumn = columns.get(Utils.MEDIA_RELEASE_HEADER);
        Boolean mediaRelease = Utils.extractBooleanCell(mediaReleaseColumn, row);

        ExcelColumn allergiesColumn = columns.get(Utils.ALLERGIES_HEADER);
        String allergies = Utils.extractStringCell(allergiesColumn, row);

        ExcelColumn canSwimColumn = columns.get(Utils.SWIM_HEADER);
        Boolean canSwim = Utils.extractBooleanCell(canSwimColumn, row);

        // null checks avoid including the headers
        if (!Objects.equals(firstName, Utils.BLANK_STRING_VALUE) &&
                !Objects.equals(lastName, Utils.BLANK_STRING_VALUE) &&
                !Objects.equals(age, Utils.BLANK_NUMERIC_VALUE)) {
            camperBuilder.location(location)
                    .firstName(firstName)
                    .lastName(lastName)
                    .shirtSize(shirtSize)
                    .shoeSize(shoeSize)
                    .maturity(maturity)
                    .age(age)
                    .gender(gender)
                    .mediaRelease(mediaRelease)
                    .allergies(allergies)
                    .canSwim(canSwim);
            campers.add(camperBuilder.build());
        }
    }

    private XSSFCellStyle getDefaultCellStyle() {
        return Utils.createCellStyle(workbook,
                BorderStyle.THIN,
                HorizontalAlignment.CENTER,
                (short)11,
                "Calibri",
                Font.COLOR_NORMAL,
                true);
    }

    private XSSFCellStyle getWarningCellStyle() {
        return Utils.createCellStyle(workbook,
                BorderStyle.THIN,
                HorizontalAlignment.CENTER,
                (short)11,
                "Calibri",
                Font.COLOR_RED,
                true);
    }

    private XSSFCellStyle getTitleCellStyle() {
        return Utils.createCellStyle(workbook,
                BorderStyle.MEDIUM,
                HorizontalAlignment.CENTER,
                (short)12,
                "Calibri",
                Font.COLOR_NORMAL,
                true);
    }

    private void setupColumnTable() {
        columnNameCol.setCellValueFactory(new PropertyValueFactory<>("columnHeader"));
        columnNumberCol.setCellValueFactory(new PropertyValueFactory<>("columnNumber"));
    }
}
