package model;

public record ExcelColumn(String columnHeader, Integer columnNumber) {

    public String getColumnHeader() {
        return columnHeader;
    }

    public Integer getColumnNumber() {
        return columnNumber;
    }
}
